package com.ekoprojekt;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class SubActivity extends ListActivity {

	ArrayList<String> odpadki = new ArrayList<String>();
	static String chosenGarbageCategory = "";
	static DBEngine de;
 
	@Override
	public void onCreate(Bundle savedInstanceState) {

		de = DBEngine.getInstance(SubActivity.this);
		de = de.open();
		
		new DatabaseFiller().fillDB(SubActivity.this);		
		new FormsFiller().fillGarbageList(odpadki);
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		TextView tv = new TextView(getApplicationContext());
		tv.setText("Odpadki");
		LinearLayout layout = new LinearLayout(this);
		layout.addView(tv);
		setListAdapter(new ArrayAdapter<String>(this, R.layout.sub, odpadki));

		ListView listView = getListView();
		listView.setTextFilterEnabled(true);

		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				chosenGarbageCategory = odpadki.get(position).toLowerCase();
				
				Intent intent = new Intent(getApplicationContext(), CompanyList.class);
				startActivity(intent);
			}
		});
	}
	
	@Override
	public void onDestroy() {
	    if(de != null) de.close();
	    super.onDestroy();
	}
}