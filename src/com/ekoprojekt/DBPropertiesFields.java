package com.ekoprojekt;

public class DBPropertiesFields {
	
	//Właściwości bazy danych
	final String DEBUG_TAG = "SqLiteTodoManager";
	final int DB_VERSION = 1;
    final String DB_NAME = "eco_database.db";
    final String DB_COMPANY_TABLE = "company";
    final String DB_GARBAGE_TABLE = "garbage";
    final String DB_COMPANY_GARBAGE_TABLE = "company_garbage";
    
    //Właściwości poszczególnych tabel
    
    //Tabela FIRMY
    final String ID_COMPANY_KEY = "id_company";
    final String ID_COMPANY_OPTIONS = "INTEGER PRIMARY KEY";
    final int ID_COMPANY_COLUMN = 0;
    
    final String COMP_NAME_KEY = "company_name";
    final String COMP_NAME_OPTIONS = "TEXT NOT NULL";
    final int COMP_NAME_COLUMN = 1;
    
    final String DESCR_KEY = "description";
    final String DESCR_OPTIONS = "TEXT NULL DEFAULT 'Brak opisu'";
    final int DESCR_COLUMN = 2;
    
    final String PHONE_KEY = "phone";
    final String PHONE_OPTIONS = "TEXT NULL DEFAULT 'B/D'";
    final int PHONE_COLUMN = 3;
    
    final String FAX_KEY = "fax";
    final String FAX_OPTIONS = "TEXT NULL DEFAULT 'B/D'";
    final int FAX_COLUMN = 4;
    
    final String CITY_KEY = "city";
    final String CITY_OPTIONS = "TEXT NOT NULL";
    final int CITY_COLUMN = 5;
    
    final String ADDRESS_KEY = "address";
    final String ADDRESS_OPTIONS = "TEXT NOT NULL";
    final int ADDRESS_COLUMN = 6;
    
    final String POSTAL_KEY = "postal_code";
    final String POSTAL_OPTIONS = "TEXT NULL DEFAULT 'B/D'";
    final int POSTAL_COLUMN = 7;
    
    final String LONGITUDE_KEY = "longitude";
    final String LONGITUDE_OPTIONS = "REAL NOT NULL";
    final int LONGITUDE_COLUMN = 8;
    
    final String LATITUDE_KEY = "latitude";
    final String LATITUDE_OPTIONS = "REAL NOT NULL";
    final int LATITUDE_COLUMN = 9;
    
    final String EMAIL_KEY = "email";
    final String EMAIL_OPTIONS = "TEXT NULL DEFAULT 'B/D'";
    final int EMAIL_COLUMN = 10;
    
    final String WWW_KEY = "www";
    final String WWW_OPTIONS = "TEXT NULL DEFAULT 'B/D'";
    final int WWW_COLUMN = 11;
    
    final String OPEN_HOURS_KEY = "open_hours";
    final String OPEN_HOURS_OPTIONS = "TEXT NULL DEFAULT 'B/D'";
    final int OPEN_HOURS_COLUMN = 12;
    
    final String DELIVERY_KEY = "delivery";
    final String DELIVERY_OPTIONS = "TEXT NULL DEFAULT 'B/D'";
    final int DELIVERY_COLUMN = 13;
    
    
    //Tabela Rodzaje Odpadków
    final String ID_GARBAGE_KEY = "id_garbage";
    final String ID_GARBAGE_OPTIONS = "INTEGER PRIMARY KEY";
    final int ID_GARBAGE_COLUMN = 0;
    
    final String GARBAGE_NAME_KEY = "garbage_name";
    final String GARBAGE_NAME_OPTIONS = "TEXT NOT NULL";
    final int GARBAGE_NAME_COLUMN = 1;
    
    
    //Tabela Rodzaje odpadków w poszczególnych firmach    
    final String ID_COMPANY_GARBAGE_KEY = "id_company_garbage";
    final String ID_COMPANY_GARBAGE_OPTIONS = "INTEGER PRIMARY KEY AUTOINCREMENT";
    final int ID_COMPANY_GARBAGE_COLUMN = 0;
    
    final String ID_COMP_FK_KEY = "id_company";
    final String ID_COMP_OPTIONS = "INTEGER NOT NULL";
    final int ID_COMP_COLUMN = 1;
    
    final String ID_GARB_FK_KEY = "id_garbage";
    final String ID_GARB_OPTIONS = "INTEGER NOT NULL";
    final int ID_GARB_COLUMN = 2;
    
    final String COMPANY_FK = "FOREIGN KEY(id_company) REFERENCES company(id_company)";
    final String GARBAGE_FK = "FOREIGN KEY(id_garbage) REFERENCES company(id_garbage)";

    
     final String CREATE_COMPANY_TABLE =
            "CREATE TABLE " + DB_COMPANY_TABLE + "( " +
            ID_COMPANY_KEY + " " + ID_COMP_OPTIONS + ", " +
            COMP_NAME_KEY + " " + COMP_NAME_OPTIONS + ", " +
            DESCR_KEY + " " + DESCR_OPTIONS + ", " +
            PHONE_KEY + " " + PHONE_OPTIONS + ", " +
            FAX_KEY + " " + FAX_OPTIONS + ", " +
            CITY_KEY + " " + CITY_OPTIONS + ", " +
            ADDRESS_KEY + " " + ADDRESS_OPTIONS + ", " +
            POSTAL_KEY + " " + POSTAL_OPTIONS + ", " +
            LONGITUDE_KEY + " " + LONGITUDE_OPTIONS + ", " +
            LATITUDE_KEY + " " + LATITUDE_OPTIONS + ", " +
            EMAIL_KEY + " " + EMAIL_OPTIONS + ", " +
            WWW_KEY + " " + WWW_OPTIONS + ", " +
            OPEN_HOURS_KEY + " " + OPEN_HOURS_OPTIONS + ", " +
            DELIVERY_KEY + " " + DELIVERY_OPTIONS +
            ");";
    
     final String DROP_COMPANY_TABLE = "DROP TABLE IF EXISTS " + DB_COMPANY_TABLE;
    
    
     final String CREATE_GARBAGE_TABLE =
            "CREATE TABLE " + DB_GARBAGE_TABLE + "( " +
            ID_GARBAGE_KEY + " " + ID_GARBAGE_OPTIONS + ", " +
            GARBAGE_NAME_KEY + " " + GARBAGE_NAME_OPTIONS +
            ");";
    
     final String DROP_GARBAGE_TABLE = "DROP TABLE IF EXISTS " + DB_GARBAGE_TABLE;
    
    
     final String CREATE_COMPANY_GARBAGE_TABLE =
            "CREATE TABLE " + DB_COMPANY_GARBAGE_TABLE + "( " +
            ID_COMPANY_GARBAGE_KEY + " " + ID_COMPANY_GARBAGE_OPTIONS + ", " +
            ID_COMP_FK_KEY + " " + ID_COMP_OPTIONS + ", " +
            ID_GARB_FK_KEY + " " + ID_GARB_OPTIONS + ", " +
            COMPANY_FK + ", " +
            GARBAGE_FK +
            ");";
    
     final String DROP_COMPANY_GARBAGE_TABLE = "DROP TABLE IF EXISTS " + DB_COMPANY_GARBAGE_TABLE;

}
