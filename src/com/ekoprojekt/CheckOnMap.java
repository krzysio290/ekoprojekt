package com.ekoprojekt;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

public class CheckOnMap extends FragmentActivity  {
	// static final LatLng MELBOURNE = new LatLng(-37.81319, 144.96298);
	// private Marker melbourne;
	private GoogleMap mMap; 
	private LocationManager locationManager;
	private static final long MIN_TIME = 4;
	private static final float MIN_DISTANCE = 10;
	private static final String TAG = null;
	private static LatLng place = new LatLng(51.80044149653985, 19.413421154022217);
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test);
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		mMap = mapFragment.getMap();

		//addLocationPoint(mMap, -37.81319, 144.96298, "Melbourne", "Population: 4,137,400");
		ObjectAccess oa = new DMLMethods().selectCompanyData(CompanyList.chosenCompanyID);
		addLocationPoint(mMap, oa.getLongitude(), oa.getLatitude(), oa.getCompanyName(), oa.getOpenHours());
		place = new LatLng(oa.getLongitude(), oa.getLatitude());
		setMyLocation(mMap);
//		LatLng myPosition = new LatLng(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude());
//		if(mMap.getMyLocation()==null) {
//			Toast.makeText(getApplicationContext(), "Is null...", Toast.LENGTH_SHORT).show();
//		} else {
//			Toast.makeText(getApplicationContext(), "Isn't null...", Toast.LENGTH_SHORT).show();
//		}
		
		/**********************/
//		LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//	    LocationListener ll = new Mylocationlistener();
//	    // ---Get the status of GPS---
//	    boolean isGPS = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
//	    // If GPS is not enable then it will be on
//	    if(!isGPS)
//	    {
//	        Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
//	        intent.putExtra("enabled", true);
//	         sendBroadcast(intent);
//	    }
//	    lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (android.location.LocationListener) ll);

	    /**********************/
		
		
		
//		LatLngBounds meAndCompanyView = new LatLngBounds.Builder().include(myPosition).include(place).build();
		
		CameraPosition cameraPosition = new CameraPosition.Builder().target(place).zoom((float) 11.00).build();
		mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//		try {
//			mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(meAndCompanyView, 20));
//		} catch(Exception e) {
//			Log.e("Maps", e.getMessage());
//		}
	}


	private void setMyLocation(GoogleMap mMap) {
		mMap.setMyLocationEnabled(true);
	}

	private void addLocationPoint(GoogleMap mMap, Double positonX, Double positonY, String title, String snippet) {
		LatLng Company = new LatLng(positonX, positonY);
		mMap.addMarker(new MarkerOptions().position(Company).title(title).snippet(snippet).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher)));

	}

}

class Mylocationlistener implements LocationListener {
    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            // ---Get current location latitude, longitude, altitude & speed ---

            Log.d("LOCATION CHANGED", location.getLatitude() + "");
            Log.d("LOCATION CHANGED", location.getLongitude() + "");
            //float speed = location.getSpeed();
            //double altitude = location.getAltitude();
            //Toast.makeText(CheckOnMap.this,"Latitude = "+ location.getLatitude() + "" +"Longitude = "+ location.getLongitude()+"Altitude = "+altitude+"Speed = "+speed,Toast.LENGTH_LONG).show();
        }
    }
}
