package com.ekoprojekt;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class CompanyList extends ListActivity {
	
	static int counter;
	static long chosenCompanyID;
	static ArrayList<Long> idList = new ArrayList<Long>();	
	ArrayList<HashMap <String, String>> firmy;
 
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		firmy = new FormsFiller().fillCompanyList(CompanyList.this, SubActivity.chosenGarbageCategory);
	
		String[] columns = new String[] { "companyName", "address", "cityAndCode" };
		int[] renderTo = new int[] { R.id.companyName, R.id.address, R.id.cityAndCode };
		ListAdapter listAdapter = new SimpleAdapter(this, firmy, R.layout.company_list_layout, columns, renderTo);
		setListAdapter(listAdapter);
	
		ListView listView = getListView();
		listView.setTextFilterEnabled(true);

		listView.setOnItemClickListener(new OnItemClickListener() {
			
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				chosenCompanyID = idList.get(firmy.indexOf(firmy.get(position)));
				
				Intent intent = new Intent(getApplicationContext(),CompanyInfo.class);
		        startActivity(intent);		  
			}
		});	
	}
}