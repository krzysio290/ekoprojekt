package com.ekoprojekt;

import java.util.ArrayList;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DMLMethods {
	
    private DBPropertiesFields dbpf = new DBPropertiesFields(); 
    private SQLiteDatabase db = SubActivity.de.getDb();
    
    
    //Metody SELECT
    public ArrayList<String> listGarbages() {
    	
    	ArrayList<String> list = new ArrayList<String>();
    	
    	String[] columns = { dbpf.GARBAGE_NAME_KEY };
    	Cursor cursor = db.query(dbpf.DB_GARBAGE_TABLE, columns, null, null, null, null, null);
    	cursor.moveToFirst();
    	
    	do {
    		String garbageType = cursor.getString(0).toUpperCase();
    		list.add(garbageType);			
    		cursor.moveToNext();
    	} while (cursor.isAfterLast() == false);  	
    	
    	return list;
    }
  
    
    public ArrayList<ObjectAccess> listCompanies(String garbageType) { 

    	ArrayList<ObjectAccess> aloa = new ArrayList<ObjectAccess>();
    	CompanyList.counter = 0;
    	CompanyList.idList.removeAll(CompanyList.idList);
    	
    	String query = "SELECT DISTINCT c." + dbpf.ID_COMPANY_KEY + ", " + dbpf.COMP_NAME_KEY + ", " + dbpf.ADDRESS_KEY + ", " + dbpf.POSTAL_KEY + ", " + dbpf.CITY_KEY + 
    				   " FROM " + dbpf.DB_COMPANY_TABLE + " c JOIN " + dbpf.DB_COMPANY_GARBAGE_TABLE + " cg ON c." + dbpf.ID_COMPANY_KEY +
    				   " = cg." + dbpf.ID_COMP_FK_KEY + " JOIN " + dbpf.DB_GARBAGE_TABLE + " g ON g." + dbpf.ID_GARBAGE_KEY +
    				   " = cg." + dbpf.ID_GARB_FK_KEY + " WHERE g." + dbpf.GARBAGE_NAME_KEY + " = ?";
    	Cursor cursor = db.rawQuery(query, new String[] { garbageType });   
    	cursor.moveToFirst();

    	do {
    		CompanyList.idList.add(cursor.getLong(0));
    		String companyName = cursor.getString(1);
    		String address = cursor.getString(2);
    		String postalCode = cursor.getString(3);
    		String city = cursor.getString(4);
    			
    		aloa.add(new ObjectAccess(companyName, city, address, postalCode));
    		
    		CompanyList.counter++;
    		cursor.moveToNext();
    	} while (cursor.isAfterLast() == false);

    	return aloa;
    }
    
    
    public ObjectAccess selectCompanyData(long id) {
    	
    	ObjectAccess oa = new ObjectAccess();
    	DBPropertiesFields dbpf = new DBPropertiesFields();
    	
    	String[] columns = { dbpf.ID_COMPANY_KEY, dbpf.COMP_NAME_KEY, dbpf.DESCR_KEY, dbpf.PHONE_KEY, dbpf.FAX_KEY, dbpf.CITY_KEY, dbpf.ADDRESS_KEY,
							 dbpf.POSTAL_KEY, dbpf.LONGITUDE_KEY, dbpf.LATITUDE_KEY, dbpf.EMAIL_KEY, dbpf.WWW_KEY, dbpf.OPEN_HOURS_KEY, 
							 dbpf.DELIVERY_KEY};
    	String where = dbpf.ID_COMPANY_KEY + "=" + id;
    	Cursor cursor1 = db.query(dbpf.DB_COMPANY_TABLE, columns, where, null, null, null, null);
    	
    	if(cursor1 != null && cursor1.moveToFirst()) {
    		long idCompany = cursor1.getLong(0);
    		String companyName = cursor1.getString(1);
    		String description = cursor1.getString(2);
    		String phone = cursor1.getString(3);
    		String fax = cursor1.getString(4);
    		String city = cursor1.getString(5);
    		String address = cursor1.getString(6);
    		String postalCode = cursor1.getString(7);
    		double longitude = cursor1.getDouble(8);
    		double latitude = cursor1.getDouble(9);
    		String email = cursor1.getString(10);
    		String www = cursor1.getString(11);
    		String openHours = cursor1.getString(12);
    		String deliveryInfo = cursor1.getString(13);
    		
    		oa = new ObjectAccess(idCompany, companyName, description, phone, fax, city, address, postalCode, longitude, 
		               			  latitude, email, www, openHours, deliveryInfo); 	
    		
    		String query = "SELECT g." + dbpf.GARBAGE_NAME_KEY + " FROM " + dbpf.DB_GARBAGE_TABLE + " g JOIN " + dbpf.DB_COMPANY_GARBAGE_TABLE +
    				       " cg ON g." + dbpf.ID_GARBAGE_KEY + " = cg." + dbpf.ID_GARB_FK_KEY + " WHERE cg." + dbpf.ID_COMP_FK_KEY + " = ?";
    		Cursor cursor2 = db.rawQuery(query, new String[] { id + "" });
    		cursor2.moveToFirst();
    		
    		CompanyInfo.garbageList.removeAll(CompanyInfo.garbageList);
    		
    		do {
        		String garbageType = cursor2.getString(0);
        		CompanyInfo.garbageList.add(garbageType);
    			
        		cursor2.moveToNext();
        	} while (cursor2.isAfterLast() == false);
    	}
    	
    	return oa;
    }
  
	
	//Metoda INSERT 
    public void insertToTable(ObjectAccess oa, String table) {
    	
    	ContentValues newTodoValues = new ContentValues();

        if (table.equals(dbpf.DB_COMPANY_TABLE)) {
        	newTodoValues.put(dbpf.ID_COMPANY_KEY, oa.getIdCompany());
	        newTodoValues.put(dbpf.COMP_NAME_KEY, oa.getCompanyName());
	        newTodoValues.put(dbpf.DESCR_KEY, oa.getDescritpion());
	        newTodoValues.put(dbpf.PHONE_KEY, oa.getPhone());
	        newTodoValues.put(dbpf.FAX_KEY, oa.getFax());
	        newTodoValues.put(dbpf.CITY_KEY, oa.getCity());
	        newTodoValues.put(dbpf.ADDRESS_KEY, oa.getAddress());
	        newTodoValues.put(dbpf.POSTAL_KEY, oa.getPostalCode());
	        newTodoValues.put(dbpf.LONGITUDE_KEY, oa.getLongitude());
	        newTodoValues.put(dbpf.LATITUDE_KEY, oa.getLatitude());
	        newTodoValues.put(dbpf.EMAIL_KEY, oa.getEmail());
	        newTodoValues.put(dbpf.WWW_KEY, oa.getWww());
	        newTodoValues.put(dbpf.OPEN_HOURS_KEY, oa.getOpenHours());
	        newTodoValues.put(dbpf.DELIVERY_KEY, oa.getDeliveryInfo());	     
	          
	        db.insert(dbpf.DB_COMPANY_TABLE, null, newTodoValues);	        
        }
        if (table.equals(dbpf.DB_GARBAGE_TABLE)) {
        	newTodoValues.put(dbpf.ID_GARBAGE_KEY, oa.getIdGarbage());
        	newTodoValues.put(dbpf.GARBAGE_NAME_KEY, oa.getGarbageName());
            
        	db.insert(dbpf.DB_GARBAGE_TABLE, null, newTodoValues);
        }
        else {
        	newTodoValues.put(dbpf.ID_COMPANY_GARBAGE_KEY, oa.getIdCompanyGarbage());
        	newTodoValues.put(dbpf.ID_COMP_FK_KEY, oa.getIdCompanyFk());
	        newTodoValues.put(dbpf.ID_GARB_FK_KEY, oa.getIdGarbageFk());
	        
	        db.insert(dbpf.DB_COMPANY_GARBAGE_TABLE, null, newTodoValues);
        }
    }
    
    

    //Metody UPDATE
    public boolean updateTable(String whichKey, String table, long id, String key, String value) {
    	
    	String where = whichKey + "=" + id;
    	
        ContentValues updateTodoValues = new ContentValues();
        updateTodoValues.put(key, value);
        
        return db.update(table, updateTodoValues, where, null) > 0;
    }
    
	public boolean updateTable(String whichKey, String table, long id, String key, long value) {
    	
		String where = whichKey + "=" + id;
    	
        ContentValues updateTodoValues = new ContentValues();
        updateTodoValues.put(key, value);
        
        return db.update(table, updateTodoValues, where, null) > 0;
    }
	
	public boolean updateTable(String whichKey, String table, long id, String key, double value) {
    	
		String where = whichKey + "=" + id;
    	
        ContentValues updateTodoValues = new ContentValues();
        updateTodoValues.put(key, value);
        
        return db.update(table, updateTodoValues, where, null) > 0;
    }
    
	
	//Metody DELETE
	public boolean deleteFromTable(String whichKey, String table, long id){
	    String where = whichKey + "=" + id;
	    return db.delete(table, where, null) > 0;
	}
	
	public boolean deleteAllTable(String table) {
	    return db.delete(table, null, null) > 0;
	}
	
	public boolean resetAutoincrement(String tab) {
		 String where = "name = " + tab;
		 return db.delete("sqlite_sequence", where, null) > 0;
	}
}
