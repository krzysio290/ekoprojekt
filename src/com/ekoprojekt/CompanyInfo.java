package com.ekoprojekt;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class CompanyInfo extends Activity {
	
	TextView[] companyFields = new TextView[10];
	static ArrayList<String> garbageList = new ArrayList<String>();
	  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.company_info);
				
		companyFields[0] = (TextView)findViewById(R.id.companyName);
		companyFields[1] = (TextView)findViewById(R.id.companyDescr);
		companyFields[2] = (TextView)findViewById(R.id.companyAddress);
		companyFields[3] = (TextView)findViewById(R.id.companyPhone);
		companyFields[4] = (TextView)findViewById(R.id.companyFax);
		companyFields[5] = (TextView)findViewById(R.id.companyWWW);
		companyFields[6] = (TextView)findViewById(R.id.companyMail);
		companyFields[7] = (TextView)findViewById(R.id.companyOpenHours);
		companyFields[8] = (TextView)findViewById(R.id.companyDelivery);
		companyFields[9] = (TextView)findViewById(R.id.companyBranch);
		
		new FormsFiller().fillCompanyFields(companyFields);
	}
	
	public void callMeMaybe(View view) {
		try {
	        Intent callIntent = new Intent(Intent.ACTION_CALL);
	        TextView tvPhoneNumber = (TextView)findViewById(R.id.companyPhone);
	        String extractedPhoneNumber = tvPhoneNumber.getText().toString().replaceAll("[\\(\\) -]", ""); 
	        callIntent.setData(Uri.parse("tel:".concat(extractedPhoneNumber)));
	        startActivity(callIntent);
	    } 
		catch (ActivityNotFoundException e) {
	        Log.e("FirmInfo", "Call failed", e);
	    }
	}
	public void searchMeMaybe(View view) {
		try {
			Intent intent = new Intent(getApplicationContext(), CheckOnMap.class);
			startActivity(intent);
	    } 
		catch (ActivityNotFoundException e) {
	        Log.e("sadfg", "sdfgh ", e);
	    }
	}
	
	public void navigateTo(View view) {
		try {
//			final Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("http://maps.google.com/maps?" + "saddr="+ latitude + "," + longitude + "&daddr=" + latitude + "," + longitude));
//		    intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
//		                        startActivity(intent);
			ObjectAccess oa = new DMLMethods().selectCompanyData(CompanyList.chosenCompanyID);
			//addLocationPoint(mMap, oa.getLongitude(), oa.getLatitude(), oa.getCompanyName(), oa.getOpenHours());
			//place = new LatLng(oa.getLongitude(), oa.getLatitude());
			
			//final Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("http://maps.google.com/maps?" + "saddr="+ (oa.getLongitude()+1) + "," + (oa.getLatitude()-1) + "&daddr=" + oa.getLongitude() + "," + oa.getLatitude()));
			final Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("http://maps.google.com/maps?" + "daddr=" + oa.getLongitude() + "," + oa.getLatitude()));
		    intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
		                        startActivity(intent);
	    } 
		catch (ActivityNotFoundException e) {
	        Log.e("sadfg", "sdfgh ", e);
	    }
	}
}