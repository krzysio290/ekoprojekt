package com.ekoprojekt;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.widget.TextView;

public class FormsFiller {
	
	
	public void fillGarbageList(ArrayList<String> odpadki) {
		
		ArrayList<String> list = new DMLMethods().listGarbages();		
		for (int i=0; i<list.size(); i++) odpadki.add(list.get(i));
	}
	
	
	public ArrayList<HashMap<String , String>> fillCompanyList(Context cont, String garbageCategory) {
		
		DMLMethods dm = new DMLMethods();
		ArrayList<ObjectAccess> aloa = new ArrayList<ObjectAccess>();
		ArrayList<HashMap<String , String>> alhm = new ArrayList<HashMap<String , String>>();

		aloa = dm.listCompanies(SubActivity.chosenGarbageCategory);

		for (int i=0; i<CompanyList.counter; i++) {
			HashMap<String, String> hm = new HashMap<String, String>();
			ObjectAccess oa = aloa.get(i);
			
			hm.put("companyName", oa.getCompanyName());
			hm.put("address", oa.getAddress());
			hm.put("cityAndCode", oa.getPostalCode() + ", " + oa.getCity());
			
			alhm.add(hm);
		}
		
		return alhm;
	}
	
	public void fillCompanyFields(TextView[] companyFields) {
		
        ObjectAccess oa = new DMLMethods().selectCompanyData(CompanyList.chosenCompanyID);
        
        companyFields[0].setText(oa.getCompanyName());
        companyFields[1].setText(oa.getDescritpion().equals("NULL") ? "Brak" : oa.getDescritpion());
        companyFields[2].setText(oa.getAddress() + "\r\n" + (oa.getPostalCode().equals("NULL") ? "-" : oa.getPostalCode()) + ", " + oa.getCity());
        companyFields[3].setText(oa.getPhone().equals("NULL") ? "B/D" : oa.getPhone());
        companyFields[4].setText(oa.getFax().equals("NULL") ? "B/D" : oa.getFax());       
        companyFields[5].setText(oa.getWww().equals("NULL") ? "B/D" : oa.getWww());
        companyFields[6].setText(oa.getEmail().equals("NULL") ? "B/D" : oa.getEmail());     
        companyFields[7].setText(oa.getOpenHours().equals("NULL") ? "B/D" : oa.getOpenHours());     
        companyFields[8].setText(oa.getDeliveryInfo().equals("NULL") ? "B/D" : oa.getDeliveryInfo());     
        
        String garbages = "";
        for (int i=0; i<CompanyInfo.garbageList.size(); i++) {
        	garbages = garbages + CompanyInfo.garbageList.get(i);
        	if (i != CompanyInfo.garbageList.size()-1) garbages = garbages + ", ";
        }
        
        companyFields[9].setText(garbages);
	}

}
