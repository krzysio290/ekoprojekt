package com.ekoprojekt;

public class ObjectAccess {
	
	private long idCompany;
	private String companyName;
	private String descritpion;
	private String phone;
	private String fax;
	private String city;
	private String address;
	private String postalCode;
	private double longitude;
	private double latitude;
	private String email;
	private String www;
	private String openHours;
	private String deliveryInfo;
	
	private long idGarbage;
	private String garbageName;
	
	private long idCompanyGarbage;
	private long idCompanyFk;
	private long idGarbageFk;
	
	
	public ObjectAccess() {}
	
	//Konstruktor u�ywany przy wype�nianiu "ma�ych" danych na li�cie firmy
	public ObjectAccess(String companyName, String city, String address, String postalCode) {
		this.companyName = companyName;
		this.city = city;
		this.address = address;
		this.postalCode = postalCode;
	}
    
  	public ObjectAccess(long idCompany, String companyName, String description,
			String phone, String fax, String city, String address,
			String postalCode, double longitude, double latitude, String email,
			String www, String openHours, String deliveryInfo) {
  		this.idCompany = idCompany;
		this.companyName = companyName;
		this.descritpion = description;
		this.phone = phone;
		this.fax = fax;
		this.city = city;
		this.address = address;
		this.postalCode = postalCode;
		this.longitude = longitude;
		this.latitude = latitude;
		this.email = email;
		this.www = www;
		this.openHours = openHours;
		this.deliveryInfo = deliveryInfo;
	}
  	
  	
	public ObjectAccess(long idGarbage, String garbageName) {
		this.idGarbage = idGarbage;
		this.garbageName = garbageName;
	}
	

	public ObjectAccess(long idCompanyGarbage, long idCompanyFk, long idGarbageFk) {
		this.idCompanyGarbage = idCompanyGarbage;
		this.idCompanyFk = idCompanyFk;
		this.idGarbageFk = idGarbageFk;
	}

	
	public long getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(long idCompany) {
		this.idCompany = idCompany;
	}

	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getDescritpion() {
		return descritpion;
	}


	public void setDescritpion(String descritpion) {
		this.descritpion = descritpion;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getFax() {
		return fax;
	}


	public void setFax(String fax) {
		this.fax = fax;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getPostalCode() {
		return postalCode;
	}


	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}


	public double getLongitude() {
		return longitude;
	}


	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}


	public double getLatitude() {
		return latitude;
	}


	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getWww() {
		return www;
	}


	public void setWww(String www) {
		this.www = www;
	}


	public String getOpenHours() {
		return openHours;
	}


	public void setOpenHours(String openHours) {
		this.openHours = openHours;
	}


	public String getDeliveryInfo() {
		return deliveryInfo;
	}


	public void setDeliveryInfo(String deliveryInfo) {
		this.deliveryInfo = deliveryInfo;
	}

	
	public long getIdGarbage() {
		return idGarbage;
	}
	

	public void setIdGarbage(long idGarbage) {
		this.idGarbage = idGarbage;
	}

	public String getGarbageName() {
		return garbageName;
	}


	public void setGarbageName(String garbageName) {
		this.garbageName = garbageName;
	}
	
	
	public long getIdCompanyGarbage() {
		return idCompanyGarbage;
	}

	public void setIdCompanyGarbage(long idCompanyGarbage) {
		this.idCompanyGarbage = idCompanyGarbage;
	}


	public long getIdCompanyFk() {
		return idCompanyFk;
	}


	public void setIdCompanyFk(long idCompanyFk) {
		this.idCompanyFk = idCompanyFk;
	}


	public long getIdGarbageFk() {
		return idGarbageFk;
	}


	public void setIdGarbageFk(long idGarbageFk) {
		this.idGarbageFk = idGarbageFk;
	}
}
