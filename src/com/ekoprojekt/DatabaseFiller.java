package com.ekoprojekt;

import android.content.Context;

public class DatabaseFiller {
	
	 public void fillDB(Context cont) {
	     
	     DBPropertiesFields dbpf = new DBPropertiesFields();
	     DBObjects dbo = new DBObjects();
	     DMLMethods dm = new DMLMethods();
	     
	     dm.insertToTable(dbo.oaComp1, dbpf.DB_COMPANY_TABLE);
	     dm.insertToTable(dbo.oaComp2, dbpf.DB_COMPANY_TABLE);
	     dm.insertToTable(dbo.oaComp3, dbpf.DB_COMPANY_TABLE);
	     dm.insertToTable(dbo.oaComp4, dbpf.DB_COMPANY_TABLE);
	     dm.insertToTable(dbo.oaComp5, dbpf.DB_COMPANY_TABLE);
	     dm.insertToTable(dbo.oaComp6, dbpf.DB_COMPANY_TABLE);
	     dm.insertToTable(dbo.oaComp7, dbpf.DB_COMPANY_TABLE);
	     dm.insertToTable(dbo.oaComp8, dbpf.DB_COMPANY_TABLE);
	     dm.insertToTable(dbo.oaComp9, dbpf.DB_COMPANY_TABLE);
	     
	     dm.insertToTable(dbo.oaGarb1, dbpf.DB_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaGarb2, dbpf.DB_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaGarb3, dbpf.DB_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaGarb4, dbpf.DB_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaGarb5, dbpf.DB_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaGarb6, dbpf.DB_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaGarb7, dbpf.DB_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaGarb8, dbpf.DB_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaGarb9, dbpf.DB_GARBAGE_TABLE);
	     
	     dm.insertToTable(dbo.oaCompGarb1_1, dbpf.DB_COMPANY_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaCompGarb1_2, dbpf.DB_COMPANY_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaCompGarb1_3, dbpf.DB_COMPANY_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaCompGarb1_4, dbpf.DB_COMPANY_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaCompGarb1_5, dbpf.DB_COMPANY_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaCompGarb2_1, dbpf.DB_COMPANY_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaCompGarb3_1, dbpf.DB_COMPANY_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaCompGarb4_1, dbpf.DB_COMPANY_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaCompGarb5_1, dbpf.DB_COMPANY_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaCompGarb6_1, dbpf.DB_COMPANY_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaCompGarb7_1, dbpf.DB_COMPANY_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaCompGarb8_1, dbpf.DB_COMPANY_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaCompGarb8_2, dbpf.DB_COMPANY_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaCompGarb9_1, dbpf.DB_COMPANY_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaCompGarb9_2, dbpf.DB_COMPANY_GARBAGE_TABLE);
	     dm.insertToTable(dbo.oaCompGarb9_3, dbpf.DB_COMPANY_GARBAGE_TABLE);     
	 }
	 
	 private final class DBObjects {
		    
		 	private ObjectAccess oaComp1 = new ObjectAccess(1, "Stena Recycling", "NULL",
								"(42) 611-24-15", "(42)�611-24-71", "��d�", "Pojezierska 97",
								"91-341", 51.80044149653985, 19.413421154022217, "lodz@stenarecycling.com",
								"http://stenarecycling.pl", "pn-pt: 08:00-16:00", "NULL");
		    private ObjectAccess oaComp2 = new ObjectAccess(2, "Ekozap", "NULL",
					 			"(42) 236-63-60", "(42) 236-63-58", "��d�", "Nowa 28",
					 			"90-031", 51.76454655845057, 19.481849670410156, "biuro@ekozap.pl",
					 			"http://www.ekozap.pl", "NULL", "NULL");
		    private ObjectAccess oaComp3 = new ObjectAccess(3, "AZ-BEST", "NULL",
								"(42) 239-87-10", "NULL", "��d�", "B�o�ska 15",
								"93-564", 51.74452910752815, 19.437131881713867, "az-best@o2.pl",
								"http://www.az-best.com.pl", "NULL", "NULL");
		    private ObjectAccess oaComp4 = new ObjectAccess(4, "Opal", "NULL",
					 			"509-726-240", "NULL", "��d�", "Pabianicka 152/154",
					 			"93-153", 51.720116272267, 19.44052219390869, "biuro@opal-recykling.pl",
					 			"http://www.opal-recykling.pl", "pn-pt: 07:00-17:00", "NULL");
		    private ObjectAccess oaComp5 = new ObjectAccess(5, "CRT Recykling", "NULL",
								"668-515-011", "NULL", "��d�", "Puszkina 80",
								"92-516", 51.74090190469151, 19.532103538513184, "biuro@crtrecykling.pl",
								"http://www.crtrecykling.pl", "pn-pt: 06:00-16:00", "NULL");
		    private ObjectAccess oaComp6 = new ObjectAccess(6, "A L Z L", "NULL",
					 			"(42)�656-76-52", "NULL", "��d�", "Beskidzka 149",
					 			"NULL", 51.79834487079885, 19.540772438049316, "NULL",
					 			"NULL", "NULL", "NULL");
		    private ObjectAccess oaComp7 = new ObjectAccess(7, "Us�ugi Asenizacyjne Tadeusz Rzepka", "NULL",
								"(42)�717-30-14", "NULL", "Zgierz", "Dubois 19A",
								"95-100", 51.85750389642158, 19.422175884246826, "asenizacja@op.pl",
								"NULL", "pn-pt: 07:00-16:00", "NULL");
		    private ObjectAccess oaComp8 = new ObjectAccess(8, "P.H.U. \"Ol-Kar\" Jadwiga Rytych", "NULL",
					 			"608-663-492", "NULL", "��d�", "Franciszka�ska 96",
					 			"91-431", 51.7917491528798, 19.460134506225586, "NULL",
					 			"NULL", "Pn-Pt.:�08.00-16.00 So.:�08.00-14.00", "NULL");
		    private ObjectAccess oaComp9 = new ObjectAccess(9, "Profit PTHU Milena Rubin", "NULL",
					 			"(42) 602-98-888", "(42) 642-80-03", "��d�", "�l�ska 3a",
					 			"93-155", 51.72730726865779, 19.480401277542114, "biuro@pthuprofit.pl",
					 			"www.pthuprofit.pl", "Pn-Pt.:�8.00-16.00  So.:�8.00-14.00", "NULL");
			
			
			private ObjectAccess oaGarb1 = new ObjectAccess(1, "metale");
			private ObjectAccess oaGarb2 = new ObjectAccess(2, "makulatura");
			private ObjectAccess oaGarb3 = new ObjectAccess(3, "tworzywa sztuczne");
			private ObjectAccess oaGarb4 = new ObjectAccess(4, "sprz�t elektryczny");
			private ObjectAccess oaGarb5 = new ObjectAccess(5, "odpady niebezpieczne");
			private ObjectAccess oaGarb6 = new ObjectAccess(6, "azbest/eternit");
			private ObjectAccess oaGarb7 = new ObjectAccess(7, "surowce wt�rne");
			private ObjectAccess oaGarb8 = new ObjectAccess(8, "wyw�z nieczysto�ci");
			private ObjectAccess oaGarb9 = new ObjectAccess(9, "z�om");
			
			
			private ObjectAccess oaCompGarb1_1 = new ObjectAccess(1, 1, 1);
			private ObjectAccess oaCompGarb1_2 = new ObjectAccess(2, 1, 2);
			private ObjectAccess oaCompGarb1_3 = new ObjectAccess(3, 1, 3);
			private ObjectAccess oaCompGarb1_4 = new ObjectAccess(4, 1, 4);
			private ObjectAccess oaCompGarb1_5 = new ObjectAccess(5, 1, 5);	     
			private ObjectAccess oaCompGarb2_1 = new ObjectAccess(6, 2, 3);
			private ObjectAccess oaCompGarb3_1 = new ObjectAccess(7, 3, 6);
			private ObjectAccess oaCompGarb4_1 = new ObjectAccess(8, 4, 7);
			private ObjectAccess oaCompGarb5_1 = new ObjectAccess(9, 5, 4);
			private ObjectAccess oaCompGarb6_1 = new ObjectAccess(10, 6, 8);
			private ObjectAccess oaCompGarb7_1 = new ObjectAccess(11, 7, 8);
			private ObjectAccess oaCompGarb8_1 = new ObjectAccess(12, 8, 1);
			private ObjectAccess oaCompGarb8_2 = new ObjectAccess(13, 8, 9);
			private ObjectAccess oaCompGarb9_1 = new ObjectAccess(14, 9, 1);
			private ObjectAccess oaCompGarb9_2 = new ObjectAccess(15, 9, 9);
			private ObjectAccess oaCompGarb9_3 = new ObjectAccess(16, 9, 2);
	 }
}
