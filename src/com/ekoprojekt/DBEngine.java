package com.ekoprojekt;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

public class DBEngine {
	
	private Context context;
	private DatabaseHelper dbHelper;
	private SQLiteDatabase db;
	private static DBEngine instance = null;
	private DBPropertiesFields dbpf = new DBPropertiesFields();
	
	
	private class DatabaseHelper extends SQLiteOpenHelper {

		 public DatabaseHelper(Context context, String name, CursorFactory factory, int version) {
			 super(context, name, factory, version);
		 }
				 
		 @Override
		    public void onCreate(SQLiteDatabase db) {
			 
		        db.execSQL(dbpf.CREATE_COMPANY_TABLE);
		        db.execSQL(dbpf.CREATE_GARBAGE_TABLE);
		        db.execSQL(dbpf.CREATE_COMPANY_GARBAGE_TABLE);
		    }
		 
		    @Override
		    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		        
		    	db.execSQL(dbpf.DROP_COMPANY_GARBAGE_TABLE);	 
		    	db.execSQL(dbpf.DROP_GARBAGE_TABLE);	 
		    	db.execSQL(dbpf.DROP_COMPANY_TABLE);	 
		        onCreate(db);
		    }
	}
	
	public static DBEngine getInstance(Context context) {
		if(instance == null) {
			instance = new DBEngine(context);
			return instance;
		}
		return instance;
	}
	
	//Konstruktory			
	private DBEngine(Context context) {
	    this.context = context;
	}	
	
	
	public SQLiteDatabase getDb() {
		return db;
	}

	
	//Otwieranie połączenia
	public DBEngine open() {
	    dbHelper = new DatabaseHelper(context, dbpf.DB_NAME, null, dbpf.DB_VERSION);
	    try {
	        db = dbHelper.getWritableDatabase();
	    } 
	    catch (SQLException e) {
	        db = dbHelper.getReadableDatabase();
	    }
	    return this;
	}
	
	
	//Zamykanie połączenia
    public void close() {
        dbHelper.close();
    }  
}
