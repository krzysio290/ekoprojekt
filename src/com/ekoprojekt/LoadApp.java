package com.ekoprojekt;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.ProgressBar;

public class LoadApp extends Activity {
	protected static final int TIMER_RUNTIME = 4000; // in ms --> 10s

	protected boolean mbActive;
	protected ProgressBar mProgressBar;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.start_layout);
		mProgressBar = (ProgressBar) findViewById(R.id.adprogress_progressBar);

		final Thread timerThread = new Thread() {
			@Override
			public void run() {
				mbActive = true;
				try {
					int waited = 0;
					while (mbActive && (waited < TIMER_RUNTIME)) {
						sleep(50);
						if (mbActive) {
							waited += 50;
							updateProgress(waited);
						}

					}

					Intent intent = new Intent(getApplicationContext(), SubActivity.class);
					startActivity(intent);
					finish();
				} catch (InterruptedException e) {
					// do nothing
				} finally {
					onContinue();
				}
			}
		};
		timerThread.start();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	public void updateProgress(final int timePassed) {
		if (null != mProgressBar) {
			// Ignore rounding error here
			final int progress = mProgressBar.getMax() * timePassed / TIMER_RUNTIME;
			mProgressBar.setProgress(progress);
		}
	}

	public void onContinue() {
		// perform any final actions here
	}
}